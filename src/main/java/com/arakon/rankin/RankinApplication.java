package com.arakon.rankin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RankinApplication {

	public static void main(String[] args) {
		SpringApplication.run(RankinApplication.class, args);
	}

}
