import React from 'react';
import './App.css';
import NavigationMenu from "./components/NavigationMenu";
import {BrowserRouter as Router, Route, Switch} from "react-router-dom";
import Competitions from "./components/Competitions";
import CreateCompetition from "./components/CreateCompetition/";

function App() {
    return (
        <Router>
            <NavigationMenu>
                <Switch>
                    <Route path="/competitions">
                        <Competitions competitions={[]}/>
                    </Route>
                    <Route path="/ratings">
                        <div>Ratings</div>
                    </Route>
                    <Route path="/history">
                        <div>History</div>
                    </Route>
                    <Route path="/home">
                        <div>Home</div>
                    </Route>
                    <Route path="/new">
                        <CreateCompetition/>
                    </Route>
                </Switch>
            </NavigationMenu>
        </Router>
    );
}

export default App;
