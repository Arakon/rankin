import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Tab from "@material-ui/core/Tab";
import {EmptyCard, Translation} from "../../utils";
import PhoneIcon from "@material-ui/icons/Phone";
import React from "react";
import {TabPanel} from "./TabPanel";
import {CreateNewGroupDialog} from "../CreateNewGroupContent/CreateNewGroupDialog";
import {Group, Stage} from "../../types";
import {useStyles} from "./styles";

function a11yProps(index: any) {
    return {
        id: `scrollable-force-tab-${index}`,
        "aria-controls": `scrollable-force-tabpanel-${index}`,
    };
}

type StageTabsProps = {
    activeStageIndex: number;
    stages: { [stageIndex: number]: Stage };
    handleChange(event: React.ChangeEvent<{}>, newValue: number): void;
    modifyGroup(group?: Group): void;
}

export const StageTabs: React.FunctionComponent<StageTabsProps> = (
    {
        activeStageIndex,
        handleChange,
        stages,
        modifyGroup
    }) => {
    const [dialogOpen, setDialogOpen] = React.useState(false);
    const classes = useStyles();

    const tabs = Object.values(stages).map((stage: Stage) =>
        (<Tab
            label={stage.name}
            icon={<PhoneIcon/>}
            {...a11yProps(stage.index)}
            key={stage.index}
        />)
    );

    function onCreateNewGroupClosed(group?: Group) {
        console.log(group);
        if (group) {
            modifyGroup(group);
        }
        setDialogOpen(false);
    }

    function renderGroups(): JSX.Element[] {
        return Object.values(stages[activeStageIndex].groups).map(group => (
                <div key={group.uuid}>
                    <div>{group.name}</div>
                    <div>{group.description}</div>
                </div>
            )
        );
    }

    return (
        <div className={classes.root}>
            <AppBar position="static" color="default">
                <Tabs
                    value={activeStageIndex}
                    onChange={handleChange}
                    variant="scrollable"
                    scrollButtons="on"
                    indicatorColor="primary"
                    textColor="primary"
                    aria-label="scrollable force tabs example"
                >
                    {tabs}
                    <Tab
                        label={<Translation>Create new stage</Translation>}
                        icon={<PhoneIcon/>}
                        {...a11yProps(Object.keys(stages).length)}
                    />
                </Tabs>
            </AppBar>
            <TabPanel
                value={activeStageIndex}
                index={activeStageIndex}
            >
                <div>
                    {renderGroups()}
                </div>
                <EmptyCard onClick={() => setDialogOpen(true)}>
                    Create new group
                </EmptyCard>
                <CreateNewGroupDialog
                    open={dialogOpen}
                    closeFn={onCreateNewGroupClosed}
                    title="Create new Group"
                />
            </TabPanel>
        </div>
    );
}