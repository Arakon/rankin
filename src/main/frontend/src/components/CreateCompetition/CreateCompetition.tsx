import React from "react";
import {Competition, Group, Stage} from "../../types";
import TextField from "@material-ui/core/TextField";
import {Button} from "@material-ui/core";
import SaveIcon from "@material-ui/icons/Save";
import DeleteIcon from "@material-ui/icons/Delete";
import {useStyles} from "./styles";
import {StageTabs} from "./StageTabs";

const firstStage: Stage = {
    name: "Stage 1",
    index: 0,
    description: "Describe the first stage of your competition",
    conditions: [],
    groups: {}
};

const initialCompetition: Competition = {
    name: "New competition",
    description: "Put here some description of your competition",
    stages: {
        [firstStage.index]: firstStage
    }
};

export const CreateCompetition: React.FunctionComponent = () => {
    const classes = useStyles();
    const [competition, setCompetition] = React.useState(initialCompetition);
    const [activeStage, setActiveStage] = React.useState(0);

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {

        if (!competition.stages[newValue]) {
            createNewStage();
        }
        console.log(newValue);
        setActiveStage(newValue);
    };

    function createNewStage() {
        const stages = competition.stages;
        const newStage: Stage = {
            name: `Stage ${Object.keys(stages).length + 1}`,
            index: Object.keys(stages).length,
            description: `Describe the first stage of your competition: Stage ${Object.keys(stages).length + 1}`,
            conditions: [],
            groups: {}
        };
        stages[newStage.index] = newStage;
        setCompetition({
            stages: {...stages},
            ...competition
        });
    }

    function modifyGroup(group?: Group): void {
        if (group) {
            const stages = competition.stages;
            stages[activeStage].groups[group.uuid] = group;
            setCompetition({
                stages,
                ...competition
            });
        }
    }

    return (
        <div>
            <TextField
                required
                id="competition-name"
                label="Name"
                size="medium"
            />
            <StageTabs
                activeStageIndex={activeStage}
                handleChange={handleChange}
                stages={competition.stages}
                modifyGroup={modifyGroup}
            />
            <div>
                <Button
                    variant="contained"
                    color="primary"
                    className={classes.button}
                    startIcon={<SaveIcon/>}
                >
                    Save
                </Button>
                <Button
                    variant="contained"
                    color="secondary"
                    className={classes.button}
                    startIcon={<DeleteIcon/>}
                >
                    Delete
                </Button>
            </div>
        </div>
    );
}