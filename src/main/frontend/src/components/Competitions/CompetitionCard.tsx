import React from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import {Translation} from "../../utils";
import {Competition} from "../../types";

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        float: 'left',
        marginRight: 12
    },
    pos: {
        marginTop: 6,
        marginBottom: 12,
        fontSize: 12
    },
    empty: {}
});

export type CompetitionCardProps = {
    competition: Competition;
    callbackFn: () => void;
}

export const CompetitionCard: React.FunctionComponent<CompetitionCardProps> = ({competition, callbackFn}) => {
    const classes = useStyles();

    const totalParticipantsCount = Object.values(competition.stages)
        .map(stage => Object.values(stage.groups)
            .map(group => group.participantsCount)
            .reduce((previousValue, currentValue) => previousValue + currentValue))
        .reduce((previousValue, currentValue) => previousValue + currentValue);

    const content = (
        <>
            <CardContent>
                <Typography
                    variant="h5"
                    component="h2"
                >
                    <Translation>
                        {competition.name}
                    </Translation>
                </Typography>
                <Typography
                    variant="body2"
                    component="p"
                >
                    {competition.description}
                </Typography>
                <Typography
                    className={classes.pos}
                    color="textSecondary"
                >
                    <Translation>Participants: </Translation>
                    {totalParticipantsCount}
                </Typography>
            </CardContent>
            <CardActions>
                <Button
                    size="small"
                    onClick={callbackFn}
                >
                    <Translation>Details</Translation>
                </Button>
            </CardActions>
        </>
    );

    return (
        <Card className={classes.root}>
            {content}
        </Card>
    );
}