import React from "react";
import {Competition} from "../../types";
import {CompetitionCard} from "./CompetitionCard";
import {WithTranslateWithRouterEmptyCard} from "../../utils";

export interface CompetitionsProps {
    competitions: Competition[]
}

export interface CompetitionsState {
    dialogOpen: boolean
}

export default class Competitions extends React.Component<CompetitionsProps, CompetitionsState> {

    constructor(props: CompetitionsProps) {
        super(props);
        this.state = {dialogOpen: false};
    }

    readonly redirect = () => {
        //
    }

    render(): React.ReactElement {
        const cards = this.props.competitions.length
            ? this.props.competitions.map(value => {
                return (<CompetitionCard
                    competition={value}
                    callbackFn={this.redirect}
                />);
            })
            : (<WithTranslateWithRouterEmptyCard link='/new'>New Competition</WithTranslateWithRouterEmptyCard>);

        return (
            <div>
                {cards}
            </div>
        );
    }
}