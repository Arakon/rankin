import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import Divider from "@material-ui/core/Divider";
import Typography from "@material-ui/core/Typography";
import {Button, FormControl, Icon, InputLabel, MenuItem, Select} from "@material-ui/core";
import {Condition, ConditionType, Group, SEX} from "../../types";
import Dialog from "@material-ui/core/Dialog";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import {Translation} from "../../utils";
import {TransitionProps} from "@material-ui/core/transitions";
import Slide from "@material-ui/core/Slide";
import DeleteIcon from '@material-ui/icons/Delete';
import {useStyles} from "./styles";
import {onlyNumbers} from "../../constants";
import { v4 as uuidv4 } from "uuid";

const defaultCondition: Condition = {
    type: ConditionType.SEX,
    data: "",
    value: ""
}

const Transition: any = React.forwardRef(function Transition(
    props: TransitionProps & { children?: React.ReactElement },
    ref: React.Ref<unknown>,
) {
    return <Slide direction="up" ref={ref} {...props} />;
});

type CreateNewGroupContentProps = {
    open: boolean;
    closeFn: (group?: Group) => void;
    title: string;
}

export const CreateNewGroupDialog: React.FunctionComponent<CreateNewGroupContentProps> = (props) => {
    const classes = useStyles();

    function clearState() {
        setConditions([]);
        setCurrentCondition(defaultCondition);
        setMaxCount("");
        setDescription("");
    }

    const handleClose = (save: boolean) => {
        if (save) {
            const group: Group = {
                uuid: uuidv4(),
                participantsCount: 0,
                conditions,
                description,
                maxParticipantsCount: Number.parseInt(maxCount.toString()),
                name: groupName
            }
            props.closeFn(group);
        } else {
            props.closeFn();
        }
        clearState();
    };

    const [conditions, setConditions] = useState(new Array<Condition>());
    const [currentCondition, setCurrentCondition] = useState(defaultCondition);
    const [groupName, setGroupName] = useState("");
    const [maxCount, setMaxCount] = useState<number | string>("");
    const [description, setDescription] = useState<string>("");

    function onClickAddCondition() {
        conditions.push(currentCondition);
        setConditions([...conditions]);
        setCurrentCondition(defaultCondition);
    }

    function handleConditionTypeChange(event: React.ChangeEvent<{ value: unknown }>) {
        setCurrentCondition({
            type: event.target.value as ConditionType,
            value: "",
            data: ""
        });
    }

    function handleSexChange(event: React.ChangeEvent<{ value: unknown }>) {
        setCurrentCondition({
            ...currentCondition,
            value: event.target.value as string
        });
    }

    function handleRatingChange(event: React.ChangeEvent<{ value: unknown }>) {
        setCurrentCondition({
            ...currentCondition,
            data: event.target.value as string,
        });
    }

    function handleConditionValueChange(event: React.ChangeEvent<HTMLInputElement>) {
        setCurrentCondition({
            ...currentCondition,
            value: event.target.value as string
        });
    }

    function removeCondition(index: number) {
        conditions.splice(index, 1);
        setConditions([...conditions]);
    }

    function onGroupNameChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setGroupName(event.target.value);
    }

    function onDescriptionChanged(event: React.ChangeEvent<HTMLInputElement>) {
        setDescription(event.target.value);
    }

    function onMaxCountChanged(event: React.ChangeEvent<HTMLInputElement>) {
        const value: string = event.target.value;
        if (value.search(onlyNumbers) > -1) {
            setMaxCount(value ? Number.parseInt(value) : "");
        }
    }

    function renderConditions(): JSX.Element[] {
        return conditions.map((value, index) => (
            <div key={index}>
                <span>{value.type}: </span>
                {value.data && <span>({value.data}) </span>}
                <span>{value.value}</span>
                <DeleteIcon
                    cursor="pointer"
                    color="error"
                    onClick={() => removeCondition(index)}
                />
            </div>
        ));
    }

    function renderDialogHeader(): JSX.Element {
        return <AppBar className={classes.appBar}>
            <Toolbar>
                <IconButton
                    edge="start"
                    color="inherit"
                    onClick={() => handleClose(false)}
                    aria-label="close"
                >
                    <CloseIcon/>
                </IconButton>
                <Typography variant="h6" className={classes.title}>
                    <Translation>{props.title}</Translation>
                </Typography>
                <Button autoFocus
                        color="inherit"
                        onClick={() => handleClose(true)}
                >
                    save
                </Button>
            </Toolbar>
        </AppBar>;
    }

    function renderConditionsBlock(): JSX.Element {
        return <>
            <Typography variant="h6">Conditions:</Typography>
            <div>
                {renderConditions()}
            </div>
            <div>
                <FormControl className={classes.formControl}>
                    <InputLabel id="condition-type-label">Condition</InputLabel>
                    <Select
                        labelId="condition-type-label"
                        id="condition-type"
                        value={currentCondition.type}
                        onChange={handleConditionTypeChange}
                    >
                        <MenuItem value={ConditionType.AGE}>{ConditionType.AGE}</MenuItem>
                        <MenuItem value={ConditionType.SEX}>{ConditionType.SEX}</MenuItem>
                        <MenuItem value={ConditionType.RATING}>{ConditionType.RATING}</MenuItem>
                    </Select>
                </FormControl>
                {currentCondition.type === ConditionType.RATING && <FormControl className={classes.formControl}>
                    <InputLabel id="rank-label">Rating name</InputLabel>
                    <Select
                        labelId="rank-label"
                        id="rank-select"
                        value={currentCondition.data}
                        onChange={handleRatingChange}
                    >
                        <MenuItem value="Tseh">Tseh</MenuItem>
                        <MenuItem value="Hiperion">Hiperion</MenuItem>
                        <MenuItem value="Space">Space</MenuItem>
                    </Select>
                </FormControl>}
                <FormControl className={classes.formControl}>
                    {currentCondition.type === ConditionType.SEX
                        ? <>
                            <InputLabel id="sex-label">Sex</InputLabel>
                            <Select
                                labelId="sex-label"
                                id="sex-select"
                                value={currentCondition.value}
                                onChange={handleSexChange}
                            >
                                <MenuItem value={SEX.FEMALE}>{SEX.FEMALE}</MenuItem>
                                <MenuItem value={SEX.MALE}>{SEX.MALE}</MenuItem>
                            </Select>
                        </>
                        : <TextField
                            required
                            id="condition-value"
                            label="Condition value"
                            value={currentCondition.value}
                            onChange={handleConditionValueChange}
                        />
                    }
                </FormControl>
            </div>
            <Button
                variant="contained"
                color="primary"
                onClick={onClickAddCondition}
                disabled={!currentCondition.value || (!currentCondition.data && currentCondition.type === ConditionType.RATING)}
            >
                add condition
            </Button>
        </>;
    }

    return (
        <Dialog
            fullScreen
            open={props.open}
            onClose={handleClose}
            TransitionComponent={Transition}
        >
            {renderDialogHeader()}
            <div>
                <TextField
                    required
                    id="group-name"
                    label="Group name"
                    onChange={onGroupNameChanged}
                    variant="outlined"
                />
                <Divider variant="middle"/>
                <TextField
                    id="mac-count"
                    label="Max number of participants"
                    value={maxCount}
                    onChange={onMaxCountChanged}
                    variant="outlined"
                />
                <Divider variant="middle"/>
                <TextField
                    id="description"
                    label="Description"
                    multiline
                    rows={4}
                    variant="outlined"
                    onChange={onDescriptionChanged}
                />
                <Divider variant="middle"/>
                {renderConditionsBlock()}
            </div>
        </Dialog>
    );
}