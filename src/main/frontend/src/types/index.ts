export type Competition = {
    name: string;
    description: string;
    stages: { [stageIndex: number]: Stage };
}

export enum ConditionType {
    AGE = "Age", SEX = "Sex", RATING = "Rating"
}

export enum SEX {
    MALE = "Mail", FEMALE = "Female"
}

export type Condition = {
    type: ConditionType;
    data: string;
    value: string;
}

export type Group = {
    uuid: string;
    name: string;
    conditions: Condition[];
    description: string;
    participantsCount: number;
    maxParticipantsCount: number | undefined;
}

export type Stage = {
    name: string;
    index: number;
    description: string;
    conditions: Condition[];
    groups: { [groupUuid: string]: Group };
}