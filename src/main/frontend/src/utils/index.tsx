import React, {PropsWithChildren, ReactElement} from "react";
import {makeStyles} from "@material-ui/core/styles";
import {RouteComponentProps} from "react-router-dom";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import {withRouter} from "react-router";

export function withTranslate(lang: string, Wrapped: React.FunctionComponent<any & PropsWithChildren<{}>>): React.FunctionComponent<PropsWithChildren<{}>> {
    return function (props: React.PropsWithChildren<{}>) {
        const translated = translate(lang, props.children as string);
        return (<Wrapped {...props}>{translated}</Wrapped>)
    }
}

// TODO: get language from somewhere
export const Translation: React.FunctionComponent<PropsWithChildren<{}>> = withTranslate("en", Wrapped);

export function translate(lang: string, value: string): string {
    // TODO: implement
    return value;
}

function Wrapped(props: PropsWithChildren<{}>): ReactElement {
    return (<span>{props.children}</span>);
}

const useStyles = makeStyles({
    root: {
        minWidth: 275,
        float: 'left',
        marginRight: 12
    },
    pos: {
        marginTop: 6,
        marginBottom: 12,
        fontSize: 12
    },
    empty: {}
});

export function EmptyCard(props: { onClick: () => void } & PropsWithChildren<{}>) {
    const classes = useStyles();

    return (
        <Card className={classes.root}>
            <CardActions>
                <Button
                    size="large"
                    onClick={props.onClick}
                >
                    {props.children}
                </Button>
            </CardActions>
        </Card>
    );
}

export type PropsWithLink = {
    link: string;
}

export const WithRouterEmptyCard = withRouter((props: RouteComponentProps & PropsWithLink): any => {

    const redirect = () => {
        props.history.push(props.link)
    };

    return <EmptyCard {...props} onClick={redirect}/>;
});

//i don't know how to align types
export const WithTranslateWithRouterEmptyCard: React.FunctionComponent<PropsWithChildren<{}> & PropsWithLink>
    // @ts-ignore
    = withTranslate("en", WithRouterEmptyCard);
