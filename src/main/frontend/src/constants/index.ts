export const onlyDecimals = /^\d*\.?\d*$/;
export const onlyNumbers = /^-?\d*$/;